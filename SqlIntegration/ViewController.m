//
//  ViewController.m
//  SqlIntegration
//
//  Created by Don Asok on 15/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "ViewController.h"
#import "SBJson.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *pass;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
///Success
-(void)alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:@"Back", nil];
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title=[alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Continue"])
    {
        NSLog(@"Continue......");
        [self performSegueWithIdentifier:@"createLogin" sender:self];
    }
}
//Failed
-(void)alertFailed:(NSString *)msg :(NSString *)title
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Try again" otherButtonTitles:nil, nil];
    [alert show];
    
}


- (IBAction)login:(id)sender

 {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://localhost/sonali/insert_data.php"]];
    
    //create the Method "GET" or "POST"
    [request setHTTPMethod:@"POST"];
    
    //Pass The String to server(YOU SHOULD GIVE YOUR PARAMETERS INSTEAD OF MY PARAMETERS)
    NSString *userUpdate =[NSString stringWithFormat:@"email=%@&pass=%@&  ",_email.text,_pass.text,nil];
    
    //Check The Value what we passed
    NSLog(@"the data Details is =%@", userUpdate);
    
    //Convert the String to Data
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [request setHTTPBody:data1];
    
    //Create the response and Error
    NSError *err;
    NSURLResponse *response;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
    
    //This is for Response
    NSLog(@"got response==%@", resSrt);
    if(resSrt)
    {
        NSLog(@"got response");
    }
        else
        {
            NSLog(@"faield to connect");
        }
    }


/*
 {
    @try
    {
        if ([[_email text]isEqualToString:@""] || [[_pass text]isEqualToString:@""])
        {
            [self alertFailed:@"Please enter both email and passeord" :@"Login failed!"];
        }
        else
        {
            NSString *post =[[NSString alloc]initWithFormat:@"email=%@&pass=%@",[_email text],[_pass text] ];
            NSLog(@"Post Data=%@",post);
            NSURL *url=[NSURL URLWithString:@"http://localhost/sonali/insert_data.php"];
            NSData *postdata=[post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLenght=[NSString stringWithFormat:@"%lu",(unsigned long)[postdata length]];
            
            NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLenght forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content=Type"];
            [request setHTTPBody:postdata];
            
            
            //NSError *error=[[NSError alloc]init];
            NSError *err = [NSError errorWithDomain:@"some_domain"
                                               code:100
                                           userInfo:@{
                                                      NSLocalizedDescriptionKey:@"Something went wrong"
                                                      }];
            NSHTTPURLResponse *response=nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
            NSLog(@"Response code=%ld",(long)[response statusCode]);
            
            if ([response statusCode] >=200 && [response statusCode] <300)
            {
                NSString *responsData=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                NSLog(@"Response==%@",responsData);
                
                SBJsonParser *jsonParser=[SBJsonParser new];
                NSDictionary *jsonData=(NSDictionary *)[jsonParser objectWithString:responsData error:nil];
                NSLog(@"%@",jsonData);
                NSInteger success=[(NSNumber *)[jsonData objectForKey:@"success"]integerValue];
                NSLog(@"%d",success);
                
                if (success==1)
                {
                    NSLog(@"Login Success");
                    [self alertStatus:@"Login succesfully" :@"Login Success"];
                    
                }
                else
                {
                    NSString *error_msg=(NSString *)[jsonData objectForKey:@"erroe_message"];
                    [self alertFailed:error_msg :@"Login Failed! Correct credentials"];
                }
            }
            else
            {
                if (err) NSLog(@"%@",err);
                [self alertStatus:@"Connection Failed" :@"Login Failed"];
            }
            
            
            
            
        }
    }
    @catch(NSException *e)
    {
        NSLog(@"Exception:%@",e);
        [self alertStatus:@"Login Failed" :@"Login Failed"];
    }
}
 */

@end
